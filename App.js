import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import HomeScreen from "./pages/HomeScreen"
import Landing from "./pages/landing"
import AboutPage from "./pages/AboutPage"
import InformationPage from "./pages/InformationPage"
import CovidApp from "./pages/index"


export default function App() {
  return (
    // <Landing/>
      // <HomeScreen />
  // <AboutPage/>
  // <InformationPage/>
    <CovidApp/>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
