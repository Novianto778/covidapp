import React from "react";
import { Text, View, StyleSheet, TouchableOpacity, Image } from "react-native";
import { ScrollView } from "react-native-gesture-handler";

export default function App({ navigation }) {
  return (
    <View style={styles.container}>
      <Image
        source={require("../assets/information-pic.png")}
        style={{ height: 205, width: 360 }}
      />
      <Text
        style={{
          position: "absolute",
          top: 40,
          left: 20,
          fontSize: 24,
          fontWeight: "bold",
        }}
      >
        Kenali
      </Text>
      <Text
        style={{
          position: "absolute",
          top: 70,
          left: 20,
          fontSize: 24,
          fontWeight: "bold",
        }}
      >
        COVID - 19
      </Text>
      <View style={styles.body}>
        <View style={{ paddingHorizontal: 25, paddingVertical: 30 }}>
          <ScrollView>
            <View>
              <Text style={{ fontSize: 18, fontWeight: "bold" }}>
                Apa Itu COVID - 19 ?
              </Text>
            </View>
            <View style={styles.card}>
              <View style={{ flexDirection: "row" }}>
                <Text
                  style={{
                    paddingLeft: 20,
                    fontSize: 16,
                    fontWeight: "normal",
                  }}
                >
                  Mengenal
                </Text>
                <TouchableOpacity
                  style={{ marginLeft: "auto", marginRight: 20 }}
                >
                  <Text
                    style={{
                      fontSize: 18,
                      fontWeight: "normal",
                    }}
                    onPress={() => navigation.navigate("Details")}
                  >
                    >
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
            <View style={styles.card}>
              <View style={{ flexDirection: "row" }}>
                <Text
                  style={{
                    paddingLeft: 20,
                    fontSize: 16,
                    fontWeight: "normal",
                  }}
                >
                  Mencegah
                </Text>
                <TouchableOpacity
                  style={{ marginLeft: "auto", marginRight: 20 }}
                >
                  <Text
                    style={{
                      fontSize: 18,
                      fontWeight: "normal",
                    }}
                  >
                    >
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
            <View style={styles.card}>
              <View style={{ flexDirection: "row" }}>
                <Text
                  style={{
                    paddingLeft: 20,
                    fontSize: 16,
                    fontWeight: "normal",
                  }}
                >
                  Mengobati
                </Text>
                <TouchableOpacity
                  style={{ marginLeft: "auto", marginRight: 20 }}
                >
                  <Text
                    style={{
                      fontSize: 18,
                      fontWeight: "normal",
                    }}
                  >
                    >
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
            <View style={{ width: 1, height: 15 }}></View>
            <Text style={{ fontSize: 18, fontWeight: "bold" }}>
              Pusat Bantuan COVID - 19
            </Text>
            <View style={styles.card}>
              <View style={{ flexDirection: "row" }}>
                <Text
                  style={{
                    paddingLeft: 20,
                    fontSize: 16,
                    fontWeight: "normal",
                  }}
                >
                  Hotline
                </Text>
                <TouchableOpacity
                  style={{ marginLeft: "auto", marginRight: 20 }}
                >
                  <Text
                    style={{
                      fontSize: 20,
                      fontWeight: "normal",
                      letterSpacing: 5,
                    }}
                  >
                    119
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
            <View style={styles.card}>
              <View style={{ flexDirection: "row" }}>
                <Text
                  style={{
                    paddingLeft: 20,
                    fontSize: 16,
                    fontWeight: "normal",
                  }}
                >
                  Website
                </Text>
                <TouchableOpacity
                  style={{ marginLeft: "auto", marginRight: 20 }}
                >
                  <Text
                    style={{
                      fontSize: 16,
                      fontWeight: "normal",
                    }}
                  >
                    https://covid19.go.id/
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
            <View style={{ width: 1, height: 15 }}></View>
          </ScrollView>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,

    backgroundColor: "#fff",
  },
  body: {
    width: 360,
    backgroundColor: "white",
    borderTopRightRadius: 40,
    marginTop: -30,
    zIndex: 10,
  },
  card: {
    width: 280,
    height: 50,
    backgroundColor: "white",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 3,
    borderWidth: 1,
    borderColor: "#e5e5e5",
    justifyContent: "center",
    marginTop: 10,
  },
});
