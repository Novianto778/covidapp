import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import { NavigationContainer } from "@react-navigation/native";
import {
  createButtonTabNavigation,
  createBottomTabNavigator,
} from "@react-navigation/bottom-tabs";
import Landing from "./landing";
import HomeScreen from "./HomeScreen";
import Details from "./DetailMengenal";
import InfoScreen from "./InformationPage";
import AboutPage from "./AboutPage";
import { MaterialCommunityIcons } from "@expo/vector-icons";

const AuthStack = createStackNavigator();
const Tabs = createBottomTabNavigator();
const HomeStack = createStackNavigator();
const DetailsStack = createStackNavigator();

const HomeStackScreen = () => (
  <HomeStack.Navigator>
    <HomeStack.Screen
      name="Tabs"
      component={TabsScreen}
      options={{ headerShown: false }}
    />
    <HomeStack.Screen name="Home" component={HomeScreen} />
  </HomeStack.Navigator>
);

const DetailsStackScreen = () => (
  <DetailsStack.Navigator>
    <DetailsStack.Screen
      name="Details"
      component={Details}
      options={{ title: "Mengenal" }}
    />
  </DetailsStack.Navigator>
);

const TabsScreen = () => (
  <Tabs.Navigator
    options={{
      initialRouteName: "InfoScreen",
    }}
  >
    <Tabs.Screen
      name="Home"
      component={HomeScreen}
      options={{
        tabBarLabel: "Home",
        tabBarIcon: ({ color, size }) => (
          <MaterialCommunityIcons name="home" color={color} size={size} />
        ),
      }}
    />
    <Tabs.Screen
      name="Information"
      component={InfoScreen}
      options={{ headerShown: false }}
      options={{
        tabBarLabel: "Info",
        tabBarIcon: ({ color, size }) => (
          <MaterialCommunityIcons
            name="information-outline"
            size={size}
            color={color}
          />
        ),
      }}
    />

    <Tabs.Screen
      name="About"
      component={AboutPage}
      options={{
        tabBarLabel: "Profile",
        tabBarIcon: ({ color, size }) => (
          <MaterialCommunityIcons name="account" color={color} size={size} />
        ),
      }}
    />
  </Tabs.Navigator>
);

export default () => (
  <NavigationContainer>
    <AuthStack.Navigator>
      <AuthStack.Screen
        name="Landing"
        component={Landing}
        options={{ headerShown: false }}
      />
      <AuthStack.Screen
        name="Home"
        component={HomeStackScreen}
        options={{ headerShown: false }}
      />
      <AuthStack.Screen
        name="Details"
        component={Details}
        options={{ title: "Mengenal COVID - 19" }}
      />
    </AuthStack.Navigator>
  </NavigationContainer>
);
