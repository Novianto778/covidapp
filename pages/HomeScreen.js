import React, { Component } from "react";
import { StyleSheet, Text, View, Image, StatusBar } from "react-native";
import { FontAwesome } from "@expo/vector-icons";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import { ScrollView } from "react-native-gesture-handler";

export default class HomeScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoad: false,
      data: "",
      data1:"",
    };
  }

  componentDidMount() {
    return fetch("https://api.covid19api.com/total/country/indonesia")
      .then((response) => response.json())
      .then((response) => {
        this.setState({
          isLoad: true,
          data: response.slice(-1)[0],
          data1: response.slice(-2)[0],
        });
      });
  }
  render() {
    return (
      <View style={styles.container}>
        <StatusBar />
        <View>
          <Image
            source={require("../assets/home-pic.png")}
            style={{ height: 205, width: 360 }}
          />
          <Text
            style={{
              position: "absolute",
              top: 50,
              left: 20,
              fontSize: 24,
              fontWeight: "bold",
            }}
          >
            Ayo Lawan
          </Text>
          <Text
            style={{
              position: "absolute",
              top: 80,
              left: 20,
              fontSize: 24,
              fontWeight: "bold",
              letterSpacing: 2,
            }}
          >
            COVID - 19
          </Text>
        </View>
        <View style={styles.body}>
          <View
            style={{
              paddingHorizontal: 25,
              paddingVertical: 40,
            }}
          >
            <ScrollView>
              <View>
                <Text style={{ fontSize: 18, fontWeight: "bold" }}>
                  Update Kasus COVID - 19
                </Text>
                <Text>Terakhir Update Tanggal <Text>{this.state.isLoad && this.state.data.Date.slice(0,10)}</Text> </Text>
              </View>
              <View
                style={{ marginTop: 20, flexDirection: "row", width: "50%" }}
              >
                <View style={styles.card}>
                  <MaterialCommunityIcons
                    name="bacteria"
                    size={24}
                    color="blue"
                  />
                  <Text>
                  {this.state.isLoad && this.state.data.Confirmed - this.state.data1.Confirmed}
                  </Text>
                  <Text>Positif</Text>
                </View>
                <View style={styles.card}>
                  <FontAwesome name="heart" size={24} color="red" />
                  <Text>{this.state.isLoad && this.state.data.Recovered - this.state.data1.Recovered}</Text>
                  <Text>Sembuh</Text>
                </View>
                <View style={styles.card}>
                  <FontAwesome name="warning" size={24} color="orange" />
                  <Text>{this.state.isLoad && this.state.data.Deaths - this.state.data1.Deaths}</Text>
                  <Text>Meninggal</Text>
                </View>
              </View>
              <View style={{ marginTop: 20 }}>
                <Text style={{ fontSize: 18, fontWeight: "bold" }}>
                  Total Kasus COVID - 19 Indonesia
                </Text>
              </View>
              <View style={{ flexDirection: "row" }}>
                <View style={styles.card1}>
                  <MaterialCommunityIcons
                    name="bacteria"
                    size={24}
                    color="blue"
                  />

                  <Text>Positif</Text>
                </View>
                <View style={styles.card2}>
                  <Text style={{ justifyContent: "center" }}>
                    {this.state.isLoad && this.state.data.Confirmed}
                  </Text>
                  <Text style={{ position: "absolute", right: 20, bottom: 10 }}>
                    Details >
                  </Text>
                </View>
              </View>
              <View style={{ flexDirection: "row", marginTop: 10 }}>
                <View style={styles.card3}>
                
                  <FontAwesome name="heart" size={24} color="red" />

                  <Text style={{ justifyContent: "center" }}>
                    {this.state.isLoad && this.state.data.Recovered}
                  </Text>
                  <Text>Sembuh</Text>
                </View>
                <View style={styles.card4}>
                <FontAwesome name="warning" size={24} color="orange" />
                  <Text style={{ justifyContent: "center" }}>
                  {this.state.isLoad && this.state.data.Deaths}
                  </Text>
                  
                  <Text>Meninggal</Text>
                </View>
              </View>
            </ScrollView>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  body: {
    width: "100%",
    backgroundColor: "white",
    height: 500,
    borderTopLeftRadius: 40,
    borderTopRightRadius: 40,
    marginTop: -30,
    zIndex: 10,
  },
  card1: {
    width: "30%",
    height: 100,
    backgroundColor: "white",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.15,
    shadowRadius: 2,
    elevation: 3,
    borderWidth: 1,
    borderColor: "#e5e5e5",
    justifyContent: "space-evenly",
    alignItems: "center",
  },
  card2: {
    width: "70%",
    height: 100,
    backgroundColor: "white",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.15,
    shadowRadius: 2,
    elevation: 3,
    borderWidth: 1,
    borderColor: "#e5e5e5",
    justifyContent: "space-evenly",
    alignItems: "center",
  },
  card: {
    width: 97,
    height: 97,
    backgroundColor: "white",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.15,
    shadowRadius: 2,
    elevation: 3,
    borderWidth: 1,
    borderColor: "#e5e5e5",
    justifyContent: "space-evenly",
    alignItems: "center",
    marginRight: 5,
  },
  card3: {
    width: "60%",
    height: 100,
    backgroundColor: "white",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.15,
    shadowRadius: 2,
    elevation: 3,
    borderWidth: 1,
    borderColor: "#e5e5e5",
    justifyContent: "space-evenly",
    alignItems: "center",
    marginRight: 15,
  },
  card4: {
    width: "35%",
    height: 100,
    backgroundColor: "white",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.15,
    shadowRadius: 2,
    elevation: 3,
    borderWidth: 1,
    borderColor: "#e5e5e5",
    justifyContent: "space-evenly",
    alignItems: "center",
  },
});
