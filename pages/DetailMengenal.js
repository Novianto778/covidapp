import React from 'react'
import { View, Text, StyleSheet } from 'react-native'

export default function DetailMengenal() {
  return (
    <View style={styles.container}>
      <View style={styles.card}>
        <Text style={{ fontSize: 14, fontWeight: 'bold', marginBottom: 10 }}>
          Apa itu COVID - 19 ?
        </Text>
        <Text>
          COVID-19 (coronavirus disease 2019) adalah jenis penyakit baru yang
          disebabkan oleh virus dari golongan coronavirus, yaitu SARS-CoV-2 yang
          juga sering disebut virus Corona.
        </Text>
      </View>
      <View style={{ width: 1, height: 15 }}></View>
      <View style={styles.card}>
        <Text style={{ fontSize: 14, fontWeight: 'bold', marginBottom: 10 }}>
          Gejala COVID - 19
        </Text>
        <Text>
          Gejala umum berupa demam ≥38 C, batuk kering, dan sesak napas.
        </Text>
      </View>
      <View style={{ width: 1, height: 15 }}></View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    paddingHorizontal: 30,
    alignItems: 'center',
    marginTop: 20,
  },
  card: {
    width: 300,
    backgroundColor: 'white',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.15,
    shadowRadius: 2,
    elevation: 3,
    borderWidth: 1,
    borderColor: '#e5e5e5',
    paddingVertical: 15,
    paddingHorizontal: 20,
  },
})