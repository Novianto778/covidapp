import React from 'react';
import { Text, View, StyleSheet } from 'react-native';
import { MaterialIcons } from '@expo/vector-icons';
import { FontAwesome } from '@expo/vector-icons';



export default function App() {
  return (
    <View style={styles.container}>
      <View style={{ alignItems: 'center' }}>
        <Text style={{ marginTop: 20, fontSize: 24, fontWeight: 'bold' }}>
          Tentang Saya
        </Text>
        <View style={styles.profile}>
          <MaterialIcons name="person" size={100} color="black" />
        </View>
        <Text style={{ fontSize: 20, marginTop: 10 }}>Novianto</Text>
        <Text style={{ fontSize: 16, color: '#8c8c8c' }}>Calon Developer</Text>
      </View>
      <View style={{ marginTop: 25 }}>
        <Text style={{ fontWeight: 'bold', fontSize: 20 }}>Social Media</Text>
        <View style={{ marginTop: 5, flexDirection: 'row' }}>
          <FontAwesome name="instagram" size={30} color="black" />
          <Text style={{ fontSize: 20, marginLeft: 15 }}>novianto718</Text>
        </View>
        <View style={{ flexDirection: 'row' }}>
          <FontAwesome name="github" size={30} color="black" />
          <Text style={{ fontSize: 20, marginLeft: 15 }}>novianto778</Text>
        </View>
      </View>
      <View style={{ marginTop: 25 }}>
        <Text style={{ fontWeight: 'bold', fontSize: 20 }}>Sumber</Text>
        <View style={{ marginTop: 5 }}>
          <Text style={{ fontSize: 16 }}>
            <Text style={{ fontWeight: 'bold' }}>Gambar:</Text> freepic.com
          </Text>
          <Text style={{ fontSize: 16 }}>
            <Text style={{ fontWeight: 'bold' }}>API: </Text>
            https://api.covid19api.com/total/country/indonesia
          </Text>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    paddingHorizontal: 20,
    fontFamily: 'Poppins',
  },
  profile: {
    width: 120,
    height: 120,
    backgroundColor: '#d5d5d5',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 70,
    marginTop: 25,
  },
});
