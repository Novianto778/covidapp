import React from "react";
import { StyleSheet, Text, View, TouchableOpacity, Image } from "react-native";

export default function App({ navigation }) {
  return (
    <View style={styles.container}>
      <Image
        source={require("../assets/first-page.png")}
        style={{ height: 640, width: "100%" }}
      />

      <View style={{ alignItems: "center" }}>
        <TouchableOpacity
          style={styles.button}
          onPress={() => navigation.navigate("Home")}
        >
          <Text>Mulai</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    backgroundColor: "#ecf0f1",
  },
  button: {
    position: "absolute",
    bottom: 80,
    paddingHorizontal: 50,
    paddingVertical: 10,
    backgroundColor: "white",
    borderRadius: 20,
  },
});
